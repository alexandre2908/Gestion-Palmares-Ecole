package Gestion_Palmares.Utils;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ResourceBundle;

public class Windows_Opening {
    String package_name = "Gestion_Palmares";
    String viewPath_Folder = "Views"; //view path folder inside the package

    public static void close_window(ActionEvent eventHandler){
        ((Node)(eventHandler.getSource())).getScene().getWindow().hide();
    }
    public void OpenNew_Windows(ActionEvent eventHandler, ResourceBundle resources, String view_name, String title_window){
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource(package_name + "/"+viewPath_Folder+"/"+view_name+".fxml"), resources);
            Stage stage = new Stage();
            stage.setTitle(title_window);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
            // Hide this current window (if this is what you want)
            ((Node)(eventHandler.getSource())).getScene().getWindow().hide();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void OpenNew_Full_Windows(ActionEvent eventHandler, ResourceBundle resources, String view_name, String title_window){
        Parent root;
        try {
            System.out.println(package_name + "/"+viewPath_Folder+"/"+view_name+".fxml");
            root = FXMLLoader.load(getClass().getClassLoader().getResource(package_name + "/"+viewPath_Folder+"/"+view_name+".fxml"), resources);
            Stage stage = new Stage();
            stage.setTitle(title_window);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setMaximized(true);
            stage.setResizable(false);
            stage.show();
            // Hide this current window (if this is what you want)
            ((Node)(eventHandler.getSource())).getScene().getWindow().hide();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void OpenNew_FloatingWindow(Event eventHandler, ResourceBundle resources, String view_name, String title_window){
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource(package_name + "/"+viewPath_Folder+"/"+view_name+".fxml"), resources);
            Stage stage = new Stage();
            stage.setTitle(title_window);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();
            // Hide this current window (if this is what you want)
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void OpenNew_FloatingWindowAndClose(ActionEvent eventHandler, ResourceBundle resources, String view_name, String title_window){
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource(package_name + "/"+viewPath_Folder+"/"+view_name+".fxml"), resources);
            Stage stage = new Stage();
            stage.setTitle(title_window);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();

            // Hide this current window (if this is what you want)
            ((Node)(eventHandler.getSource())).getScene().getWindow().hide();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
