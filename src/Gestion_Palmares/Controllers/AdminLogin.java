package Gestion_Palmares.Controllers;

import Gestion_Palmares.Models.Operation_Database;
import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminLogin  implements Initializable{
    public JFXTextField username;
    public JFXButton btn_login;
    public JFXButton btn_fermer;
    public JFXPasswordField password;
    public Label retour_connect;
    Windows_Opening windows_opening = new Windows_Opening();
    Operation_Database operation_database = new Operation_Database();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btn_fermer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        btn_login.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                boolean login_test = operation_database.Login_Verify(username.getText(), password.getText());
                if (login_test){
                    Metadata_Palmares.username = username.getText();
                    Metadata_Palmares.isAdmin = true;
                    windows_opening.OpenNew_Full_Windows(event,resources,"Main_Window","Main Window");
                }else {
                    retour_connect.setText("Nom d'utilisateur ou Mots de pass incorrect");
                }
            }
        });
    }
}
