package Gestion_Palmares.Controllers;

import Gestion_Palmares.Models.Eleves;
import Gestion_Palmares.Models.Operation_Database;
import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class Main_WindowsAll implements Initializable{
    public TableView<Eleves> tableView;
    public TableColumn<Eleves, String> matricule;
    public TableColumn<Eleves, String> nom;
    public TableColumn<Eleves, String> postnom;
    public TableColumn<Eleves, String> prenom;
    public TableColumn<Eleves, String> classe;
    public TableColumn<Eleves, String> date_naiss;
    public Tab Tab_Operation;
    public JFXButton btn_ajouterEleves;
    public Label username_recup;
    public Label date_today;
    public JFXTabPane main_tabpane;
    public Tab tab_listeEleve;
    public JFXTextField txt_recherche;

    private ObservableList<Eleves> eleves_listes = FXCollections.observableArrayList();
    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {



        if (!Metadata_Palmares.isAdmin){
            Tab_Operation.setDisable(true);
            username_recup.setText("Chers Visiteur");
        }else {
            username_recup.setText(Metadata_Palmares.username);
        }

        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        date_today.setText(timeStamp);

        btn_ajouterEleves.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindow(event, resources, "ajouterEleves", "Ajouter Eleves");
            }
        });
        tab_listeEleve.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                loadData_eleves();
            }
        });

        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Metadata_Palmares.eleves = tableView.getSelectionModel().getSelectedItem();
                windows_opening.OpenNew_FloatingWindow(event, resources, "OnClick_Eleves", "choix");

            }
        });

        initCol_eleves();
        loadData_eleves();

        FilteredList<Eleves> filteredList = new FilteredList<Eleves>(eleves_listes, p->true);

        txt_recherche.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                filteredList.setPredicate(eleves -> {
                    if (newValue == null || newValue.isEmpty()){
                        return true;
                    }
                    //comparer avec le nom
                    String lowercasefilter = newValue.toLowerCase();

                    if (eleves.getNom_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }else if (eleves.getPrenom_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }else if (eleves.getPostnom_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }else if (eleves.getMatricule_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }else if (eleves.getDatenaiss_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }else if (eleves.getClasse_eleves().toLowerCase().contains(lowercasefilter)){
                        return true;
                    }
                    return false;
                });
            }
        });

        SortedList<Eleves> sortedList = new SortedList<Eleves>(filteredList);

        sortedList.comparatorProperty().bind(tableView.comparatorProperty());

        tableView.setItems(sortedList);

    }

    private void initCol_eleves() {
        matricule.setCellValueFactory(new PropertyValueFactory<>("matricule_eleves"));
        nom.setCellValueFactory(new PropertyValueFactory<>("nom_eleves"));
        postnom.setCellValueFactory(new PropertyValueFactory<>("postnom_eleves"));
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom_eleves"));
        classe.setCellValueFactory(new PropertyValueFactory<>("classe_eleves"));
        date_naiss.setCellValueFactory(new PropertyValueFactory<>("datenaiss_eleves"));
    }

    public void loadData_eleves() {
        eleves_listes.clear();

        ResultSet resultSet = operation_database.getall_eleves();
        try {
            while (resultSet.next()) {
                int id_eleves = resultSet.getInt("id_eleves");
                String matricule_eleves = resultSet.getString("matricule_eleves");
                String nom_eleve = resultSet.getString("nom_eleves");
                String postnom_eleves = resultSet.getString("postnom_eleves");
                String prenom_eleves = resultSet.getString("prenom_eleves");
                String classe_eleves = resultSet.getString("classe_eleves");
                String datenaiss_eleves = resultSet.getString("datenaiss_eleves");

                eleves_listes.add(new Eleves(id_eleves, nom_eleve, postnom_eleves, prenom_eleves, datenaiss_eleves, classe_eleves, matricule_eleves));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        tableView.setItems(eleves_listes);
    }

}
