package Gestion_Palmares.Controllers;

import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class Intermediares implements Initializable{
    public JFXButton btnVoir_Plamares;
    public JFXButton btnAjouter_Palmares;
    public JFXButton btn_fermer;
    public AnchorPane main_panel;

    private Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (!Metadata_Palmares.isAdmin){
            btnAjouter_Palmares.setDisable(true);
        }
        btn_fermer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        btnVoir_Plamares.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindow(event, resources, "View_Palmares", "Voir Palmares");
            }
        });

        btnAjouter_Palmares.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindow(event, resources, "ajouter_palmares", "Ajouter Palmares");
            }
        });
    }
}
