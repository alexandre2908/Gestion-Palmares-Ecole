package Gestion_Palmares.Controllers;

import Gestion_Palmares.Models.Eleves;
import Gestion_Palmares.Models.Operation_Database;
import Gestion_Palmares.Models.Palmares_eleves;
import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class AjouterPalmares implements Initializable{
    public JFXTextField txt_anneescolaire;
    public JFXTextField txtclasse_eleve;
    public JFXTextField txt_pourc;
    public JFXButton btn_ajouter;
    public JFXButton btn_annule;
    public Label txt_retour;
    public TextArea txt_commentaires;
    public Label txt_nomcomplet;

    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Eleves eleves = Metadata_Palmares.eleves;
        String nom_complet = eleves.getNom_eleves() + " " + eleves.getPostnom_eleves()+ " "+ eleves.getPrenom_eleves();
        txt_nomcomplet.setText(nom_complet);

        btn_annule.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        btn_ajouter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Eleves eleves = Metadata_Palmares.eleves;
                Palmares_eleves palmares_eleves = new Palmares_eleves(eleves.getId_eleves(), txt_anneescolaire.getText(), txt_pourc.getText(), txtclasse_eleve.getText(), txt_commentaires.getText());
                int resultat = operation_database.insert_palmares(palmares_eleves, eleves);
                if (resultat == 1){
                    Windows_Opening.close_window(event);
                }else {
                    txt_retour.setText("Erreur lors de l'insertion du palmares");
                }
            }
        });

    }
}
