package Gestion_Palmares.Controllers;

import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class FirstWinController implements Initializable{

    public JFXButton btn_visiteur;
    public JFXButton btn_admin;
    public JFXButton btn_fermer;
    public AnchorPane main_panel;
    Windows_Opening windows_opening = new Windows_Opening();

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btn_fermer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        btn_admin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                windows_opening.OpenNew_FloatingWindowAndClose(event, resources, "Admin_Login", "Adminnistrateur");
            }
        });

        btn_visiteur.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Metadata_Palmares.isAdmin = false;
                windows_opening.OpenNew_Full_Windows(event, resources, "Main_Window", "Adminnistrateur");
            }
        });
    }
}
