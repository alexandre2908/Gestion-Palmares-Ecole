package Gestion_Palmares.Controllers;

import Gestion_Palmares.Models.Eleves;
import Gestion_Palmares.Models.Operation_Database;
import Gestion_Palmares.Utils.Utils;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class AjouterEleves implements Initializable{
    public JFXTextField txtnom_eleve;
    public JFXTextField txtpostnom_eleve;
    public JFXTextField txtclasse_eleve;
    public JFXTextField txtprenom_eleve;
    public JFXTextField txtdate_eleve;
    public JFXButton btn_ajouter;
    public JFXButton btn_annule;
    public Label txt_retour;


    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();
    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btn_annule.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        btn_ajouter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int count = operation_database.getcount_eleves();
                String matricule = Utils.CalculeMatricule(txtnom_eleve.toString(), txtprenom_eleve.toString(), count);
                String nom = txtnom_eleve.getText();
                String postnom = txtpostnom_eleve.getText();
                String prenom = txtprenom_eleve.getText();
                String date_naiss = txtdate_eleve.getText();
                String classe = txtclasse_eleve.getText();

                // Creation d'un eleve

                Eleves eleves = new Eleves(nom, postnom, prenom, date_naiss, classe, matricule);
                int resultat = operation_database.insert_eleves(eleves);
                if (resultat == 1){
                    Windows_Opening.close_window(event);
                }else {
                    txt_retour.setText("Erreur lors de l'ajout d'un eleve");
                }
            }
        });
    }
}
