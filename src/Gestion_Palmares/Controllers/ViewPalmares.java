package Gestion_Palmares.Controllers;

import Gestion_Palmares.Models.Eleves;
import Gestion_Palmares.Models.Operation_Database;
import Gestion_Palmares.Models.Palmares_eleves;
import Gestion_Palmares.Utils.Metadata_Palmares;
import Gestion_Palmares.Utils.Windows_Opening;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ViewPalmares implements Initializable{
    public TableView table_palmares;
    public TableColumn column_annee;
    public TableColumn column_classe;
    public TableColumn column_pourc;
    public TableColumn column_commentaire;
    public Label txt_nomEleves;
    public JFXButton btn_fermer;

    private ObservableList<Palmares_eleves> palmares_eleves = FXCollections.observableArrayList();
    private Operation_Database operation_database = new Operation_Database();
    private Windows_Opening windows_opening = new Windows_Opening();


    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Eleves eleves = Metadata_Palmares.eleves;
        String nom_complet = eleves.getNom_eleves() + " " + eleves.getPostnom_eleves()+ " "+ eleves.getPrenom_eleves();
        txt_nomEleves.setText(nom_complet);

        btn_fermer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Windows_Opening.close_window(event);
            }
        });

        initCol_eleves();
        loadData_eleves();

    }

    private void initCol_eleves() {
        column_annee.setCellValueFactory(new PropertyValueFactory<>("annee_scolaire"));
        column_classe.setCellValueFactory(new PropertyValueFactory<>("classe"));
        column_commentaire.setCellValueFactory(new PropertyValueFactory<>("commentaires"));
        column_pourc.setCellValueFactory(new PropertyValueFactory<>("pourcentage"));

    }

    public void loadData_eleves() {
        palmares_eleves.clear();
        int id_eleves_emp = Metadata_Palmares.eleves.getId_eleves();
        ResultSet resultSet = operation_database.getallPalmares_perId(id_eleves_emp);
        try {
            while (resultSet.next()) {
                int id_eleves = resultSet.getInt("id_eleves");
                int id_palmares = resultSet.getInt("id_palmares");
                String annee_scolaire = resultSet.getString("annee_scolaire");
                String pourcentage = resultSet.getString("pourcentage");
                String classe = resultSet.getString("classe");
                String commentaires = resultSet.getString("commentaires");
                palmares_eleves.add(new Palmares_eleves(id_palmares, id_eleves, annee_scolaire, pourcentage, classe, commentaires));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        table_palmares.setItems(palmares_eleves);
    }
}
