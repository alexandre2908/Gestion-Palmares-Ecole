package Gestion_Palmares.Models;

import java.sql.*;

public class Operation_Database {
    private Connection connection = Database_Connection.getconnection();

    public boolean Login_Verify(String username, String password) {
        boolean value = false;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM administrateur WHERE username_admin='" + username + "' AND password_admin='" + password + "'");
            resultSet.last();
            int count = resultSet.getRow();
            value = count != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;

    }

    public ResultSet getall_eleves() {
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM eleves");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
    public ResultSet getallPalmares_perId(int id_eleves) {
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM palmares_eleves WHERE id_eleves="+id_eleves);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }


    public int insert_eleves(Eleves eleves) {
        int resultat = 0;
        try {
            String sql = "INSERT INTO eleves (nom_eleves, postnom_eleves, prenom_eleves, datenaiss_eleves, matricule_eleves, classe_eleves) VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, eleves.getNom_eleves());
            statement.setString(2, eleves.getPostnom_eleves());
            statement.setString(3, eleves.getPrenom_eleves());
            statement.setString(4, eleves.getDatenaiss_eleves());
            statement.setString(5, eleves.getMatricule_eleves());
            statement.setString(6, eleves.getClasse_eleves());
            resultat = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }
    public int insert_palmares(Palmares_eleves palmares_eleves, Eleves eleves) {
        int resultat = 0;
        try {
            String sql = "INSERT INTO palmares_eleves (id_eleves, annee_scolaire, pourcentage, classe, commentaires) VALUES (?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, eleves.getId_eleves());
            statement.setString(2, palmares_eleves.getAnnee_scolaire());
            statement.setString(3, palmares_eleves.getPourcentage());
            statement.setString(4, palmares_eleves.getClasse());
            statement.setString(5, palmares_eleves.getCommentaires());
            resultat = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }


    public int getcount_eleves() {
        ResultSet resultSet = null;
        int count = 0;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT count(*) AS count_num FROM eleves");
            while (resultSet.next()) {
                count = resultSet.getInt("count_num");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        count++;

        return count;
    }
}