package Gestion_Palmares.Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


class Database_Connection {
    private static Connection connection;
    private static String url="jdbc:mysql://localhost:3306/gestion_palmares";
    private static String username="root";
    private static String password="";
    static Connection getconnection() {
        try {
            if (connection==null){
                Class.forName("com.mysql.jdbc.Driver");
                Database_Connection.connection=connect(url,username,password);
            }else {
                return Database_Connection.connection;
            }
        }catch (ClassNotFoundException | SQLException exc){
            exc.printStackTrace();
        }
        return Database_Connection.connection;
    }

    private static Connection connect(String url, String user, String password) throws SQLException{
        Database_Connection.connection= DriverManager.getConnection(url,user,password);
        return connection;
    }


}