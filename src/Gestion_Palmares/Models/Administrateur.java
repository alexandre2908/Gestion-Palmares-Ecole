package Gestion_Palmares.Models;

public class Administrateur {
  private Long id_admin;
  private String username_admin;
  private String password_admin;

  public Long getId_admin() {
    return id_admin;
  }

  public void setId_admin(Long id_admin) {
    this.id_admin = id_admin;
  }

  public String getUsername_admin() {
    return username_admin;
  }

  public void setUsername_admin(String username_admin) {
    this.username_admin = username_admin;
  }

  public String getPassword_admin() {
    return password_admin;
  }

  public void setPassword_admin(String password_admin) {
    this.password_admin = password_admin;
  }
}
