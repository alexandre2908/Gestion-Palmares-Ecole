package Gestion_Palmares.Models;

public class Eleves {
  private int id_eleves;
  private String nom_eleves;
  private String postnom_eleves;
  private String prenom_eleves;
  private String datenaiss_eleves;
  private String classe_eleves;
  private String matricule_eleves;

  public Eleves(int id_eleves, String nom_eleves, String postnom_eleves, String prenom_eleves, String datenaiss_eleves, String classe_eleves, String matricule_eleves) {
    this.id_eleves = id_eleves;
    this.nom_eleves = nom_eleves;
    this.postnom_eleves = postnom_eleves;
    this.prenom_eleves = prenom_eleves;
    this.datenaiss_eleves = datenaiss_eleves;
    this.classe_eleves = classe_eleves;
    this.matricule_eleves = matricule_eleves;
  }

  public Eleves(String nom_eleves, String postnom_eleves, String prenom_eleves, String datenaiss_eleves, String classe_eleves, String matricule_eleves) {
    this.nom_eleves = nom_eleves;
    this.postnom_eleves = postnom_eleves;
    this.prenom_eleves = prenom_eleves;
    this.datenaiss_eleves = datenaiss_eleves;
    this.classe_eleves = classe_eleves;
    this.matricule_eleves = matricule_eleves;
  }

  public int getId_eleves() {
    return id_eleves;
  }

  public void setId_eleves(int id_eleves) {
    this.id_eleves = id_eleves;
  }

  public String getNom_eleves() {
    return nom_eleves;
  }

  public void setNom_eleves(String nom_eleves) {
    this.nom_eleves = nom_eleves;
  }

  public String getPostnom_eleves() {
    return postnom_eleves;
  }

  public void setPostnom_eleves(String postnom_eleves) {
    this.postnom_eleves = postnom_eleves;
  }

  public String getPrenom_eleves() {
    return prenom_eleves;
  }

  public void setPrenom_eleves(String prenom_eleves) {
    this.prenom_eleves = prenom_eleves;
  }

  public String getDatenaiss_eleves() {
    return datenaiss_eleves;
  }

  public void setDatenaiss_eleves(String datenaiss_eleves) {
    this.datenaiss_eleves = datenaiss_eleves;
  }

  public String getMatricule_eleves() {
    return matricule_eleves;
  }

  public void setMatricule_eleves(String matricule_eleves) {
    this.matricule_eleves = matricule_eleves;
  }

  public String getClasse_eleves() {
    return classe_eleves;
  }

  public void setClasse_eleves(String classe_eleves) {
    this.classe_eleves = classe_eleves;
  }
}
