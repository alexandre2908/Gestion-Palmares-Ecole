package Gestion_Palmares.Models;

public class Palmares_eleves {
  private int id_palmares;
  private int id_eleves;
  private String annee_scolaire;
  private String pourcentage;
  private String classe;
  private String commentaires;

  public Palmares_eleves(int id_palmares, int id_eleves, String annee_scolaire, String pourcentage, String classe, String commentaires) {
    this.id_palmares = id_palmares;
    this.id_eleves = id_eleves;
    this.annee_scolaire = annee_scolaire;
    this.pourcentage = pourcentage;
    this.classe = classe;
    this.commentaires = commentaires;
  }

  public Palmares_eleves(int id_eleves, String annee_scolaire, String pourcentage, String classe, String commentaires) {
    this.id_eleves = id_eleves;
    this.annee_scolaire = annee_scolaire;
    this.pourcentage = pourcentage;
    this.classe = classe;
    this.commentaires = commentaires;
  }

  public int getId_palmares() {
    return id_palmares;
  }

  public void setId_palmares(int id_palmares) {
    this.id_palmares = id_palmares;
  }

  public int getId_eleves() {
    return id_eleves;
  }

  public void setId_eleves(int id_eleves) {
    this.id_eleves = id_eleves;
  }

  public String getAnnee_scolaire() {
    return annee_scolaire;
  }

  public void setAnnee_scolaire(String annee_scolaire) {
    this.annee_scolaire = annee_scolaire;
  }

  public String getPourcentage() {
    return pourcentage;
  }

  public void setPourcentage(String pourcentage) {
    this.pourcentage = pourcentage;
  }

  public String getClasse() {
    return classe;
  }

  public void setClasse(String classe) {
    this.classe = classe;
  }

  public String getCommentaires() {
    return commentaires;
  }

  public void setCommentaires(String commentaires) {
    this.commentaires = commentaires;
  }
}
